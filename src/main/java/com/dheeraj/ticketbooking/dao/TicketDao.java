package com.dheeraj.ticketbooking.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dheeraj.ticketbooking.model.Ticket;

@Repository 
//By default this will get connection object automatically witout any manual code
/*
 * For every table ==> CRUD operation
 * c ==> Create  update ==> Update
 * R ==> Read    delete ==> Delete
 * 
 * Framework is creating a repository with some default methods for above
 * CURD Operations
 * 
 * CRUD Repository expects two parameters
 * 1) Entity Name 2) Data Type of PrimaryKey of the Entity
 */
public interface TicketDao extends CrudRepository<Ticket, Integer> {
	
	//This repository will internally will have default methods for CRUD Operations

	
}
