package com.dheeraj.ticketbooking.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dheeraj.ticketbooking.model.Ticket;
import com.dheeraj.ticketbooking.service.TicketService;

@RestController
@RequestMapping(value = "/tickets")
//http://localhost:8080/tickets ==> TicketController tckCtlObj= ioc.getTicketController();
public class TicketController {

	/*
	 * JAX RS ==> Java API for XML Rest Services ===> JSON
	 * 
	 * WEB SERVICE ==> Heterogeneous communication
	 * 
	 * SOAP SERVICE ==> SOAP PROTOCOL ==> Tightly coupled ==> we can't easily call
	 * REST SERVICE ==> HTTP PROTOCOL ==> Loosely coupled ==> Every one can easily
	 * use http
	 * 
	 * @Controller ==> Our input request will come from jsp page only .. BOTH
	 * frontend and MiddleWare should be in same language
	 * 
	 * @RestController ==> Our input request can come from any where from outside
	 * world like angular,React,GO, PHP etc.,
	 * 
	 * VERBS ==> GET ==> To GET THE DATA ==> POST ==> To POST THE DATA ==> PUT ==>
	 * To UPDATE THE DATA ==> DELETE ==> To DELETE THE DATA
	 * 
	 * REQUEST ==> HTTPREQUEST and RESPONSE will be returned in the HTTP Response
	 * 
	 * Json is the communication between client and server
	 * 
	 * By default java to json and json to java conversions taken care by framework
	 * 
	 * It uses Jackson library for the conversion
	 * 
	 * @GetMapping ==> @Get + @RequestMapping
	 * 
	 * @PostMapping ==> @Post + @RequestMapping
	 */

	@Autowired
	TicketService ticketServiceObj;

	// Getting all Tickets
	@GetMapping(value = "/all")
	// Output Iterable<Ticket> will be converted to Json and given to client
	public Iterable<Ticket> getAllTickets() {
		return ticketServiceObj.getAllTickets();
	}

	// Get a Ticket
	@GetMapping(value = "/{ticketId}")
	// http://localhost:8080/tickets/1 ==> 1st Ticket
	// To write dynamic variable in requestMapping we need to use { }
	// @PathVariable ==>Reading the variable form URI/URL
	public Ticket getTicket(@PathVariable("ticketId") Integer ticketId) {
		return ticketServiceObj.getTicket(ticketId);
	}

	// Update Ticket
	@PutMapping(value = "/{ticketId}/{newEmail}")
	// http://localhost:8080/1/abc@gm.com
	public Ticket updateTicket(@PathVariable("ticketId") Integer id, @PathVariable("newEmail") String emailId) {
		return ticketServiceObj.updateTicket(id, emailId);
	}
	
	//create Ticket
	@PostMapping(value = "/create")
	//http://localhost:8080/tickets/create
	public Ticket createTicket(@RequestBody Ticket ticketObj) {
		
		return ticketServiceObj.createTicket(ticketObj);
	}
	
	//Delete Ticket
	@DeleteMapping(value = "/{ticketId}")
	public void deleteTicket(@PathVariable("ticketId") Integer id) {
		ticketServiceObj.deleteTicket(id);
	}
}
