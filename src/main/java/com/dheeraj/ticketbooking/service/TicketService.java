package com.dheeraj.ticketbooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dheeraj.ticketbooking.dao.TicketDao;
import com.dheeraj.ticketbooking.model.Ticket;

@Service
//Mainly used for achieving Transaction Management
//any other external tools connectivity like rabbitmq,kafka.. etc
public class TicketService {
	// In this layer we are going to write create, update, delete , get ticket
	// methods
	@Autowired
	private TicketDao ticketDao; // what ever the memory reference of this class it will get that reference

	public Ticket createTicket(Ticket ticket) {
		// save method is going to trigger insert query to the database
		// save method is also going to trigger update query to the database
		// if ticket is not having value to ticketId, ticketId value is not in database
		// ==> insert
		// if ticketid is there in the database ==> It will trigger update statement to
		// the database
		return ticketDao.save(ticket);
	}

	public Iterable<Ticket> getAllTickets() {
		// find all ==> It will trigger query ==> select * from tbl_ticket
		// Entire ResultSet will be converted into any CollectionDataType<Entity>
		return ticketDao.findAll();
	}

	public Ticket getTicket(Integer id) {
		// select * from tbl_ticket where ticked_id=ticketId;
		return ticketDao.findById(id).orElse(new Ticket());
	}

	public void deleteTicket(Integer id) {
		ticketDao.deleteById(id);
	}

	public Ticket updateTicket(Integer ticketId, String email) {
		Ticket matchedTicket = getTicket(ticketId);
		if (null != matchedTicket) {
			matchedTicket.setEmail(email);
		}
		return ticketDao.save(matchedTicket);
	}

}
